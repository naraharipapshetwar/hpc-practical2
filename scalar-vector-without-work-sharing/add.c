#include<iostream>
#include<omp.h>
using namespace std;

int main(){
	
	int i,n;
	scanf("%d",&n);
	
	
	int a[n],c[n];
	for(i=0;i<n;i++)
		scanf("%d",&a[i]);
	
	int num;
	scanf("%d",&num);
	
	printf("%d\n",omp_get_num_threads());
	#pragma omp parallel default(shared)
	{
 		int id, i, Nthrds, istart, iend;
 		
 		id = omp_get_thread_num();
 		Nthrds = omp_get_num_threads();
 		
 		istart = id*n/Nthrds;
	 	iend = (id+1)*n/Nthrds;
	 	
 		if(id == Nthrds-1) iend = n;
 		for(i = istart; i<iend; i++) 
		 	c[i] = num+a[i];
	}
	
	
 	for(i=0;i<n;i++)
 		cout<<c[i]<<" ";
 	
}
